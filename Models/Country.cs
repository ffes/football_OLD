using System;

namespace Football.Models
{
	public class Country
	{
		public string CountryID { get; set; }
		public string CountryName { get; set; }
		public string GroupID { get; set; }
	}
}
