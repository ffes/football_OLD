using System.Collections.Generic;

namespace Football.Models
{
	public class Group
	{
		public string GroupID { get; set; }
		public List<Country> Countries { get; set; }
		public bool Completed { get; set; }
	}
}
