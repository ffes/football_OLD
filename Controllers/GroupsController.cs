﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Football.Models;

namespace Football.Controllers
{
	[ApiController]
	[Route("api/input/[controller]")]
	public class GroupsController : ControllerBase
	{
		private readonly ILogger<GroupsController> _logger;

		public GroupsController(ILogger<GroupsController> logger)
		{
			_logger = logger;
		}

		[HttpGet]
		public IEnumerable<Group> Get()
		{
			var grp = new Group
			{
				GroupID = "A",
				Countries = new List<Country> {
					new Country {
						CountryID = "nl",
						CountryName = "Netherlands",
						GroupID = "A"
					},
					new Country {
						CountryID = "de",
						CountryName = "Germany",
						GroupID = "A"
					}
				},
				Completed = false
			};

			var lst = new List<Group>();
			lst.Add(grp);
			lst.Add(grp);
			return lst;
		}
	}
}
