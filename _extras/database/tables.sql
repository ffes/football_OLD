-----------------------------------------------------------------------------
-- The countries that takes part in Euro 2020, including the group they    --
-- play their first round in                                               --
-----------------------------------------------------------------------------

PRAGMA foreign_keys = ON;

DROP TABLE IF EXISTS Countries;
CREATE TABLE Countries (
	CountryID		nvarchar(2) NOT NULL,
	CountryName		nvarchar(50) NOT NULL,
	GroupID			nvarchar(1) NOT NULL,
    PRIMARY KEY(CountryID)
);

-----------------------------------------------------------------------------
-- The phases the tournament has                                           --
-- It also contains the number of answers in the phase to be able to       --
-- check if a player has filled in all the answers and the numbers of      --
-- point you'll get for a correct prediction in that phase                 --
-----------------------------------------------------------------------------

DROP TABLE IF EXISTS Phases;
CREATE TABLE Phases (
	PhaseID			integer NOT NULL,
	PhaseName		nvarchar(50) NOT NULL,
	Knockout		integer NOT NULL,
	Answers			integer NOT NULL,
	Points			integer NOT NULL,
	OrderBy			integer NOT NULL,
	PRIMARY KEY(PhaseID)
);

-----------------------------------------------------------------------------
-- The matches played in the tournament                                    --
-----------------------------------------------------------------------------

DROP TABLE IF EXISTS Matches;
CREATE TABLE Matches (
	MatchID			integer NOT NULL,
	PhaseID			integer NOT NULL,
	GroupID			nvarchar(1),
	HomeID			nvarchar(2),
	AwayID			nvarchar(2),
	MatchDate		datetime,
	MatchResult		integer,
	Stadium			nvarchar(50),
    PRIMARY KEY(MatchID),
	FOREIGN KEY(PhaseID) REFERENCES Phases(PhaseID),
	FOREIGN KEY(HomeID) REFERENCES Countries(CountryID) ON UPDATE CASCADE,
	FOREIGN KEY(AwayID) REFERENCES Countries(CountryID) ON UPDATE CASCADE
);
CREATE INDEX Matches_PhaseID ON Matches(PhaseID);

-----------------------------------------------------------------------------
-- The users participating in our game                                     --
-----------------------------------------------------------------------------

DROP TABLE IF EXISTS Users;
CREATE TABLE Users (
	UserID		integer NOT NULL,
	EMail		varchar(50) NOT NULL,
	Password	varchar(100) NOT NULL,
	RealName	varchar(50) NOT NULL,
	NickName	varchar(50) NOT NULL,
	SigninKey	varchar(50) NOT NULL,
	Enabled		bit DEFAULT 0,
	UserLevel	int NOT NULL DEFAULT 0,
	PRIMARY KEY (UserID)
);
CREATE UNIQUE INDEX Users_EMail ON Users(EMail);

-----------------------------------------------------------------------------
-- The answers given by the users                                          --
-----------------------------------------------------------------------------

DROP TABLE IF EXISTS Answers;
CREATE TABLE Answers (
	AnswerID	integer NOT NULL,
	UserID		integer NOT NULL,
	MatchID		integer NOT NULL,
	Answer		integer DEFAULT NULL,
	PhaseID		integer NOT NULL,
	CountryID	nvarchar(2) DEFAULT NULL,
	Points		integer NOT NULL DEFAULT 0,
	PRIMARY KEY (AnswerID),
	FOREIGN KEY(UserID) REFERENCES Users(UserID),
	FOREIGN KEY(MatchID) REFERENCES Matches(MatchID),
	FOREIGN KEY(PhaseID) REFERENCES Phases(PhaseID),
	FOREIGN KEY(CountryID) REFERENCES Countries(CountryID)
);
