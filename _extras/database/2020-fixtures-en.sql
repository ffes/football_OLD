-----------------------------------------------------------------------------
-- The countries that takes part in Euro 2020                              --
-----------------------------------------------------------------------------

PRAGMA foreign_keys = ON;

INSERT INTO Countries VALUES ('tr','Turkey','A');
INSERT INTO Countries VALUES ('it','Italy','A');
INSERT INTO Countries VALUES ('wa','Wales','A');
INSERT INTO Countries VALUES ('ch','Switzerland','A');

INSERT INTO Countries VALUES ('dk','Denmark','B');
INSERT INTO Countries VALUES ('fi','Finland','B');
INSERT INTO Countries VALUES ('be','Belgium','B');
INSERT INTO Countries VALUES ('ru','Russia','B');

INSERT INTO Countries VALUES ('nl','Netherlands','C');
INSERT INTO Countries VALUES ('ua','Ukraine','C');
INSERT INTO Countries VALUES ('at','Austria','C');
INSERT INTO Countries VALUES ('c4','Winner play-offs D or A','C');

INSERT INTO Countries VALUES ('en','England','D');
INSERT INTO Countries VALUES ('hr','Croatia','D');
INSERT INTO Countries VALUES ('d3','Winner play-offs C','D');
INSERT INTO Countries VALUES ('cz','Czechia','D');

INSERT INTO Countries VALUES ('es','Spain','E');
INSERT INTO Countries VALUES ('se','Sweden','E');
INSERT INTO Countries VALUES ('pl','Poland','E');
INSERT INTO Countries VALUES ('e4','Winner play-offs B','E');

INSERT INTO Countries VALUES ('f1','Winner play-offs A or D','F');
INSERT INTO Countries VALUES ('pt','Portugal','F');
INSERT INTO Countries VALUES ('fr','France','F');
INSERT INTO Countries VALUES ('de','Germany','F');

-----------------------------------------------------------------------------
-- The phases the tournament has                                           --
-----------------------------------------------------------------------------

INSERT INTO Phases VALUES (1,'Group Stage',0,36,1,1);
INSERT INTO Phases VALUES (2,'Round of 16',1,16,2,2);
INSERT INTO Phases VALUES (3,'Quarter-finals',1,8,4,3);
INSERT INTO Phases VALUES (4,'Semi-finals',1,4,7,4);
INSERT INTO Phases VALUES (5,'Final',1,2,10,5);
INSERT INTO Phases VALUES (6,'Champion',1,1,20,6);

-----------------------------------------------------------------------------
-- The matches played in the tournament                                    --
-----------------------------------------------------------------------------

--- Group A
INSERT INTO Matches VALUES ( 1,1,'A','tr','it','2020-06-12 21:00:00',NULL,'Stadio Olimpico, Rome');
INSERT INTO Matches VALUES ( 2,1,'A','wa','ch','2020-06-13 15:00:00',NULL,'Olympic Stadium, Baku');
INSERT INTO Matches VALUES (13,1,'A','tr','wa','2020-06-17 21:00:00',NULL,'Olympic Stadium, Baku');
INSERT INTO Matches VALUES (14,1,'A','it','ch','2020-06-17 21:00:00',NULL,'Stadio Olimpico, Rome');
INSERT INTO Matches VALUES (25,1,'A','it','wa','2020-06-21 18:00:00',NULL,'Stadio Olimpico, Rome');
INSERT INTO Matches VALUES (26,1,'A','ch','tr','2020-06-21 18:00:00',NULL,'Olympic Stadium, Baku');

--- Group B
INSERT INTO Matches VALUES ( 3,1,'B','dk','fi','2020-06-13 18:00:00',NULL,'Parken Stadium, Copenhagen');
INSERT INTO Matches VALUES ( 4,1,'B','be','ru','2020-06-13 21:00:00',NULL,'Krestovsky Stadium, Saint Petersburg');
INSERT INTO Matches VALUES (15,1,'B','fi','ru','2020-06-17 15:00:00',NULL,'Krestovsky Stadium, Saint Petersburg');
INSERT INTO Matches VALUES (16,1,'B','dk','be','2020-06-18 18:00:00',NULL,'Parken Stadium, Copenhagen');
INSERT INTO Matches VALUES (27,1,'B','ru','dk','2020-06-22 21:00:00',NULL,'Parken Stadium, Copenhagen');
INSERT INTO Matches VALUES (28,1,'B','fi','be','2020-06-22 21:00:00',NULL,'Krestovsky Stadium, Saint Petersburg');

--- Group C
INSERT INTO Matches VALUES ( 5,1,'C','at','c4','2020-06-14 18:00:00',NULL,'National Arena, Bucharest');
INSERT INTO Matches VALUES ( 6,1,'C','nl','ua','2020-06-14 21:00:00',NULL,'Johan Cruyff ArenA, Amsterdam');
INSERT INTO Matches VALUES (17,1,'C','ua','c4','2020-06-18 15:00:00',NULL,'National Arena, Bucharest');
INSERT INTO Matches VALUES (18,1,'C','nl','at','2020-06-18 21:00:00',NULL,'Johan Cruyff ArenA, Amsterdam');
INSERT INTO Matches VALUES (29,1,'C','c4','nl','2020-06-18 21:00:00',NULL,'Johan Cruyff ArenA, Amsterdam');
INSERT INTO Matches VALUES (30,1,'C','ua','at','2020-06-18 21:00:00',NULL,'National Arena, Bucharest');

--- Group D
INSERT INTO Matches VALUES ( 7,1,'D','en','hr','2020-06-14 15:00:00',NULL,'Wembley Stadium, London');
INSERT INTO Matches VALUES ( 8,1,'D','d3','cz','2020-06-15 15:00:00',NULL,'Hampden Park, Glasgow');
INSERT INTO Matches VALUES (19,1,'D','hr','cz','2020-06-19 18:00:00',NULL,'Hampden Park, Glasgow');
INSERT INTO Matches VALUES (20,1,'D','en','d3','2020-06-19 21:00:00',NULL,'Wembley Stadium, London');
INSERT INTO Matches VALUES (31,1,'D','hr','d3','2020-06-23 21:00:00',NULL,'Hampden Park, Glasgow');
INSERT INTO Matches VALUES (32,1,'D','cz','en','2020-06-23 21:00:00',NULL,'Wembley Stadium, London');

--- Group E
INSERT INTO Matches VALUES ( 9,1,'E','es','se','2020-06-15 21:00:00',NULL,'San Mamés Stadium, Bilbao');
INSERT INTO Matches VALUES (10,1,'E','pl','e4','2020-06-15 18:00:00',NULL,'Dublin Arena, Dublin');
INSERT INTO Matches VALUES (21,1,'E','se','e4','2020-06-19 15:00:00',NULL,'Dublin Arena, Dublin');
INSERT INTO Matches VALUES (22,1,'E','es','pl','2020-06-20 21:00:00',NULL,'San Mamés Stadium, Bilbao');
INSERT INTO Matches VALUES (33,1,'E','e4','es','2020-06-24 21:00:00',NULL,'San Mamés Stadium, Bilbao');
INSERT INTO Matches VALUES (34,1,'E','es','pl','2020-06-24 21:00:00',NULL,'Dublin Arena, Dublin');

--- Group F
INSERT INTO Matches VALUES (11,1,'F','f1','pt','2020-06-16 18:00:00',NULL,'Puskás Aréna, Budapest');
INSERT INTO Matches VALUES (12,1,'F','fr','de','2020-06-16 21:00:00',NULL,'Allianz Arena, Munich');
INSERT INTO Matches VALUES (23,1,'F','f1','fr','2020-06-20 15:00:00',NULL,'Puskás Aréna, Budapest');
INSERT INTO Matches VALUES (24,1,'F','pt','de','2020-06-20 18:00:00',NULL,'Allianz Arena, Munich');
INSERT INTO Matches VALUES (35,1,'F','pt','fr','2020-06-24 21:00:00',NULL,'Puskás Aréna, Budapest');
INSERT INTO Matches VALUES (36,1,'F','de','f1','2020-06-24 21:00:00',NULL,'Allianz Arena, Munich');

--- Knockout - Round of 16
INSERT INTO Matches VALUES (37,2,NULL,NULL,NULL,'2020-06-27 21:00:00',NULL,'Wembley Stadium, London');
INSERT INTO Matches VALUES (38,2,NULL,NULL,NULL,'2020-06-27 18:00:00',NULL,'Johan Cruyff ArenA, Amsterdam');
INSERT INTO Matches VALUES (39,2,NULL,NULL,NULL,'2020-06-28 21:00:00',NULL,'San Mamés Stadium, Bilbao');
INSERT INTO Matches VALUES (40,2,NULL,NULL,NULL,'2020-06-28 18:00:00',NULL,'Puskás Aréna, Budapest');
INSERT INTO Matches VALUES (41,2,NULL,NULL,NULL,'2020-06-29 21:00:00',NULL,'National Arena, Bucharest');
INSERT INTO Matches VALUES (42,2,NULL,NULL,NULL,'2020-06-29 18:00:00',NULL,'Parken Stadium, Copenhagen');
INSERT INTO Matches VALUES (43,2,NULL,NULL,NULL,'2020-06-30 21:00:00',NULL,'Hampden Park, Glasgow');
INSERT INTO Matches VALUES (44,2,NULL,NULL,NULL,'2020-06-30 18:00:00',NULL,'Dublin Arena, Dublin');

--- Knockout - Quarter-finals
INSERT INTO Matches VALUES (45,3,NULL,NULL,NULL,'2020-07-03 18:00:00',NULL,'Krestovsky Stadium, Saint Petersburg');
INSERT INTO Matches VALUES (46,3,NULL,NULL,NULL,'2020-07-03 21:00:00',NULL,'Allianz Arena, Munich');
INSERT INTO Matches VALUES (47,3,NULL,NULL,NULL,'2020-07-04 18:00:00',NULL,'Olympic Stadium, Baku');
INSERT INTO Matches VALUES (48,3,NULL,NULL,NULL,'2020-07-04 21:00:00',NULL,'Stadio Olimpico, Rome');

--- Knockout - Semi-finals
INSERT INTO Matches VALUES (49,4,NULL,NULL,NULL,'2020-07-07 21:00:00',NULL,'Wembley Stadium, London');
INSERT INTO Matches VALUES (50,4,NULL,NULL,NULL,'2020-07-08 21:00:00',NULL,'Wembley Stadium, London');

--- Knockout - Final
INSERT INTO Matches VALUES (51,5,NULL,NULL,NULL,'2020-07-12 21:00:00',NULL,'Wembley Stadium, London');

--- Champion
INSERT INTO Matches VALUES (52,6,NULL,NULL,NULL,NULL,NULL,NULL);
